import UpdatePartnerRepPhoneNumberReq from '../../src/updatePartnerRepPhoneNumberReq';
import dummy from '../dummy';

/*
 tests
 */
describe('UpdatePartnerRepPhoneNumberReq class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepPhoneNumberReq(
                        null,
                        dummy.phoneNumber
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');

        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepPhoneNumberReq(
                    expectedId,
                    dummy.phoneNumber
                );

            /*
             assert
             */
            const actualId =
                objectUnderTest.id;

            expect(actualId).toEqual(expectedId);

        });
        it('throws if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepPhoneNumberReq(
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'phoneNumber required');

        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.phoneNumber;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepPhoneNumberReq(
                    dummy.userId,
                    expectedPhoneNumber
                );

            /*
             assert
             */
            const actualPhoneNumber =
                objectUnderTest.phoneNumber;

            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new UpdatePartnerRepPhoneNumberReq(
                    dummy.userId,
                    dummy.phoneNumber
                );

            const expectedObject =
            {
                id: objectUnderTest.id,
                phoneNumber: objectUnderTest.phoneNumber
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
