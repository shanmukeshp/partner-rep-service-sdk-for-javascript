import UpdatePartnerRepFirstNameReq from '../../src/updatePartnerRepFirstNameReq';
import dummy from '../dummy';

/*
 tests
 */
describe('UpdatePartnerRepFirstNameReq class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepFirstNameReq(
                        null,
                        dummy.firstName
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');

        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepFirstNameReq(
                    expectedId,
                    dummy.firstName
                );

            /*
             assert
             */
            const actualId =
                objectUnderTest.id;

            expect(actualId).toEqual(expectedId);

        });
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepFirstNameReq(
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');

        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepFirstNameReq(
                    dummy.userId,
                    expectedFirstName
                );

            /*
             assert
             */
            const actualFirstName =
                objectUnderTest.firstName;

            expect(actualFirstName).toEqual(expectedFirstName);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new UpdatePartnerRepFirstNameReq(
                    dummy.userId,
                    dummy.firstName
                );

            const expectedObject =
            {
                id: objectUnderTest.id,
                firstName: objectUnderTest.firstName
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
