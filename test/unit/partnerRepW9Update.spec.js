import PartnerRepW9Update from '../../src/partnerRepW9Update';
import dummy from '../dummy';

describe('PartnerRepW9Update class', () => {
    describe('constructor', () => {
        it('throws if transactionId is null', () => {
            /*
             arrange
             */
            const constructor = () => new PartnerRepW9Update(null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'transactionId required');

        });
        it('sets transactionId', () => {
            /*
             arrange
             */
            const expectedTransactionId = dummy.transactionId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepW9Update(expectedTransactionId);

            /*
             assert
             */
            const actualTransactionId = objectUnderTest.transactionId;
            expect(actualTransactionId).toEqual(actualTransactionId);
        });
    });
});