import UpdatePartnerRepPostalAddressReq from '../../src/updatePartnerRepPostalAddressReq';
import dummy from '../dummy';

/*
 tests
 */
describe('UpdatePartnerRepPostalAddressReq class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepPostalAddressReq(
                        null,
                        dummy.postalAddress
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');

        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepPostalAddressReq(
                    expectedId,
                    dummy.postalAddress
                );

            /*
             assert
             */
            const actualId =
                objectUnderTest.id;

            expect(actualId).toEqual(expectedId);

        });
        it('throws if postalAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new UpdatePartnerRepPostalAddressReq(
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'postalAddress required');

        });
        it('sets postalAddress', () => {
            /*
             arrange
             */
            const expectedPostalAddress = dummy.postalAddress;

            /*
             act
             */
            const objectUnderTest =
                new UpdatePartnerRepPostalAddressReq(
                    dummy.userId,
                    expectedPostalAddress
                );

            /*
             assert
             */
            const actualPostalAddress =
                objectUnderTest.postalAddress;

            expect(actualPostalAddress).toEqual(expectedPostalAddress);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new UpdatePartnerRepPostalAddressReq(
                    dummy.userId,
                    dummy.postalAddress
                );

            const expectedObject =
            {
                id: objectUnderTest.id,
                postalAddress: objectUnderTest.postalAddress.toJSON()
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
