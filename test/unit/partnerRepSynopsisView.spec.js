import PartnerRepSynopsisView from '../../src/partnerRepSynopsisView';
import dummy from '../dummy';

/*
 tests
 */
describe('PartnerRepSynopsisView class', () => {
    describe('constructor', () => {
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepSynopsisView(
                        null,
                        dummy.lastName,
                        dummy.userId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');
        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepSynopsisView(
                    expectedFirstName,
                    dummy.lastName,
                    dummy.userId
                );

            /*
             assert
             */
            const actualFirstName = objectUnderTest.firstName;
            expect(actualFirstName).toEqual(expectedFirstName);
        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepSynopsisView(
                        dummy.firstName,
                        null,
                        dummy.userId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');
        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepSynopsisView(
                    dummy.firstName,
                    expectedLastName,
                    dummy.userId
                );

            /*
             assert
             */
            const actualLastName = objectUnderTest.lastName;
            expect(actualLastName).toEqual(expectedLastName);
        });
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepSynopsisView(
                        dummy.firstName,
                        dummy.lastName,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepSynopsisView(
                    dummy.firstName,
                    dummy.lastName,
                    expectedId
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);
        });
    });
});
