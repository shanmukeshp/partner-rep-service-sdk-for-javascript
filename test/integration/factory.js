import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';

let constructUniqueEmailAddressCallCounter = 0;

export default {
    constructValidPartnerRepOAuth2AccessToken,
    constructUniqueEmailAddress
}

function constructValidPartnerRepOAuth2AccessToken(accountId:string = dummy.accountId,
                                                   sub:string = dummy.userId):string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": sub,
        "account_id": accountId
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructUniqueEmailAddress():string {
    return `${constructUniqueEmailAddressCallCounter++}${Date.now()}@test.com`
}