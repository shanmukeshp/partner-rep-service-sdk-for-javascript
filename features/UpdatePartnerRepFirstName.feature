Feature: Update Partner Rep First Name
  Updates the first name of the partner rep with the provided id

  Scenario: Success
    Given I provide an accessToken identifying me as the partnerRep with id partnerRepId
    And I provide a valid firstName
    When I execute updatePartnerRepFirstName
    Then my firstName is updated in the partner-rep-service