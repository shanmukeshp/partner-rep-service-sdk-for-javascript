Feature: List Partner Reps With Account Id
  Lists the partner reps with the provided account id

  Scenario: Success
    Given I provide an accountId that matches partnerReps in the partner-rep-service
    And provide an accessToken identifying me as a partner rep associated with accountId
    When I execute listPartnerRepsWithAccountId
    Then the partnerReps are returned