Feature: Get Partner Rep Bank Info Update Url
  Gets a transient url where a partner reps bank info can be updated

  Scenario: Success
    Given I provide an accessToken identifying me as the partnerRep with id partnerRepId
    And I provide a returnUrl
    When I execute getPartnerRepBankInfoUpdateUrl
    And a bankInfoUpdateUrl from the partner-rep-service is returned