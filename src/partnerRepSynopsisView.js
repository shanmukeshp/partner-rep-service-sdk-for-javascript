/**
 * The least detailed view of a partner rep
 * @class {PartnerRepSynopsisView}
 */
export default class PartnerRepSynopsisView {

    _firstName:string;

    _lastName:string;

    _id:string;

    constructor(firstName:string,
                lastName:string,
                id:string) {

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

    }

    get firstName():string {
        return this._firstName;
    }

    get lastName():string {
        return this._lastName;
    }

    get id():string {
        return this._id;
    }

    toJSON() {
        return {
            firstName: this._firstName,
            lastName: this._lastName,
            id: this._id
        };
    }

}
