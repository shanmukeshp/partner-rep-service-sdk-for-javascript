import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepSynopsisView from './partnerRepSynopsisView';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class SearchForPartnerRepWithEmailAddressFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Searches for a partner rep with the provided email address.
     * @param {string} emailAddress
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepSynopsisView>}
     */
    execute(emailAddress:string,
            accessToken:string):Promise<PartnerRepSynopsisView> {

        return this._httpClient
            .createRequest(`partner-reps`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                emailAddress: emailAddress
            })
            .send()
            .then(response => {
                if (response.content) {
                    return new PartnerRepSynopsisView(
                        response.content.firstName,
                        response.content.lastName,
                        response.content.id
                    );
                } else {
                    return null;
                }
            });
    }
}

export default SearchForPartnerRepWithEmailAddressFeature;