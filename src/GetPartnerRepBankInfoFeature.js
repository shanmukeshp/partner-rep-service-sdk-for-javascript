import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import BankInfoView from './bankInfoView';
import BankInfoViewFactory from './bankInfoViewFactory';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class GetPartnerRepBankInfoFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * gets the bank info with the provided id.
     * @param {string} partnerRepId
     * @param {string} accessToken
     * @returns {Promise.<BankInfoView>}
     */
    execute(partnerRepId:string,
            accessToken:string):Promise<BankInfoView> {

        return this._httpClient
            .createRequest(`/partner-reps/bankinfo/`+`${partnerRepId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withHeader('Content-Type', `application/json`)
            .send()
            .then(response =>
                        new BankInfoView(
                            response.content.id,
                            response.content.isExists
                        )
            );
    }
}

export default GetPartnerRepBankInfoFeature;