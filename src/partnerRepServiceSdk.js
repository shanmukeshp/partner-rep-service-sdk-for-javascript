import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import DiContainer from './diContainer';
import AddPartnerRepReq from './addPartnerRepReq';
import AddPartnerRepFeature from './addPartnerRepFeature';
import GetPartnerRepBankInfoUpdateUrlReq from './getPartnerRepBankInfoUpdateUrlReq';
import GetPartnerRepBankInfoUpdateUrlFeature from './getPartnerRepBankInfoUpdateUrlFeature';
import GetPartnerRepW9UpdateUrlReq from './getPartnerRepW9UpdateUrlReq';
import GetPartnerRepW9UpdateUrlFeature from './getPartnerRepW9UpdateUrlFeature';
import GetPartnerRepWithIdFeature from './getPartnerRepWithIdFeature';
import GetPartnerRepsWithIdsFeature from './getPartnerRepsWithIdsFeature';
import PartnerRepSynopsisView from './partnerRepSynopsisView';
import PartnerRepView from './partnerRepView';
import SearchForPartnerRepWithEmailAddressFeature from './searchForPartnerRepWithEmailAddressFeature';
import UpdatePartnerRepFirstNameFeature from './updatePartnerRepFirstNameFeature';
import UpdatePartnerRepLastNameFeature from './updatePartnerRepLastNameFeature';
import UpdatePartnerRepPhoneNumberFeature from './updatePartnerRepPhoneNumberFeature';
import UpdatePartnerRepPostalAddressFeature from './updatePartnerRepPostalAddressFeature';
import UpdatePartnerRepFirstNameReq from './updatePartnerRepFirstNameReq';
import UpdatePartnerRepLastNameReq from './updatePartnerRepLastNameReq';
import UpdatePartnerRepPhoneNumberReq from './updatePartnerRepPhoneNumberReq';
import UpdatePartnerRepPostalAddressReq from './updatePartnerRepPostalAddressReq';
import IsBankInfoExistsForPartnerRepsFeature from './isBankInfoExistsForPartnerRepsFeature';
import IsW9InfoExistsForPartnerRepsFeature from './isW9InfoExistsForPartnerRepsFeature';
import GetPartnerRepBankInfoFeature from './GetPartnerRepBankInfoFeature';
import GetPartnerRepW9InfoFeature from './GetPartnerRepW9InfoFeature';
import BankInfoView from './bankInfoView';
import W9InfoView from './w9InfoView';

/**
 * @class {PartnerRepServiceSdk}
 */
export default class PartnerRepServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {PartnerRepServiceSdkConfig} config
     */
    constructor(config:PartnerRepServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    addPartnerRep(request:AddPartnerRepReq,
                  accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(AddPartnerRepFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerRepW9UpdateUrl(request:GetPartnerRepW9UpdateUrlReq,
                             accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(GetPartnerRepW9UpdateUrlFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerRepBankInfoUpdateUrl(request:GetPartnerRepBankInfoUpdateUrlReq,
                                   accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(GetPartnerRepBankInfoUpdateUrlFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerRepWithId(id:string,
                        accessToken:string):Promise<PartnerRepView> {

        return this
            ._diContainer
            .get(GetPartnerRepWithIdFeature)
            .execute(
                id,
                accessToken
            );
    }

    getPartnerRepsWithIds(partnerRepIds:string[],
                          accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(GetPartnerRepsWithIdsFeature)
            .execute(
                partnerRepIds,
                accessToken
            );
    }

    searchForPartnerRepWithEmailAddress(emailAddress:string,
                                        accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(SearchForPartnerRepWithEmailAddressFeature)
            .execute(
                emailAddress,
                accessToken
            );

    }

    updatePartnerRepFirstName(request:UpdatePartnerRepFirstNameReq,
                              accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepFirstNameFeature)
            .execute(
                request,
                accessToken
            );
    }

    updatePartnerRepLastName(request:UpdatePartnerRepLastNameReq,
                             accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepLastNameFeature)
            .execute(
                request,
                accessToken
            );
    }

    updatePartnerRepPhoneNumber(request:UpdatePartnerRepPhoneNumberReq,
                                accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepPhoneNumberFeature)
            .execute(
                request,
                accessToken
            );
    }

    updatePartnerRepPostalAddress(request:UpdatePartnerRepPostalAddressReq,
                                  accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepPostalAddressFeature)
            .execute(
                request,
                accessToken
            );
    }

    isBankInfoExistsForPartnerReps(partnerRepIds:string[],
                                   accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(IsBankInfoExistsForPartnerRepsFeature)
            .execute(
                partnerRepIds,
                accessToken
            );
    }

    isW9InfoExistsForPartnerReps(partnerRepIds:string[],
                                 accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(IsW9InfoExistsForPartnerRepsFeature)
            .execute(
                partnerRepIds,
                accessToken
            );
    }

    isBankInfoExistsForPartnerRep(partnerRepId:string,
                                   accessToken:string):Promise<BankInfoView> {

        return this
            ._diContainer
            .get(GetPartnerRepBankInfoFeature)
            .execute(
                partnerRepId,
                accessToken
            );
    }

    isW9InfoExistsForPartnerRep(partnerRepId:string,
                                 accessToken:string):Promise<W9InfoView> {

        return this
            ._diContainer
            .get(GetPartnerRepW9InfoFeature)
            .execute(
                partnerRepId,
                accessToken
            );
    }

}
