export default class UpdatePartnerRepLastNameReq {

    _id:number;

    _lastName:string;

    /**
     * @param {number} id
     * @param {string} lastName
     */
    constructor(id:number,
                lastName:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

    }


    get id():number {
        return this._id;
    }

    get lastName():string {
        return this._lastName;
    }

    toJSON() {
        return {
            id: this._id,
            lastName: this._lastName
        };
    }
}