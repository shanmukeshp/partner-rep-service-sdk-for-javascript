import PostalAddress from 'postal-object-model';
import PartnerRepBankInfoUpdate from './partnerRepBankInfoUpdate';
import PartnerRepW9Update from './partnerRepW9Update';

/**
 * The most detailed view of a partner rep
 * @class {PartnerRepView}
 */
export default class PartnerRepView {

    _id:string;

    _firstName:string;

    _lastName:string;

    _emailAddress:string;

    _accountId:string;

    _postalAddress:PostalAddress;

    _phoneNumber:string;

    _sapVendorNumber:string;

    /**
     *
     * @param {string} id
     * @param {lastName} firstName
     * @param {string} lastName
     * @param {string} emailAddress
     * @param {string} accountId
     * @param {PostalAddress|null} [postalAddress]
     * @param {string|null} [phoneNumber]
     * @param {string|null} [sapVendorNumber]
     */
    constructor(id:string,
                firstName:string,
                lastName:string,
                emailAddress:string,
                accountId:string,
                postalAddress:PostalAddress = null,
                phoneNumber:string = null,
                sapVendorNumber:string = null) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!emailAddress) {
            throw new TypeError('emailAddress required');
        }
        this._emailAddress = emailAddress;

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        this._postalAddress = postalAddress;

        this._phoneNumber = phoneNumber;

        this._sapVendorNumber = sapVendorNumber;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {string}
     */
    get emailAddress():string {
        return this._emailAddress;
    }

    /**
     * @returns {string}
     */
    get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {PostalAddress|null}
     */
    get postalAddress():PostalAddress {
        return this._postalAddress;
    }

    /**
     * @returns {string|null}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /**
     * @returns {string|null}
     */
    get sapVendorNumber():string {
        return this._sapVendorNumber;
    }

    toJSON() {
        return {
            id: this._id,
            firstName: this._firstName,
            lastName: this._lastName,
            emailAddress: this._emailAddress,
            accountId: this._accountId,
            postalAddress: this._postalAddress ? this._postalAddress.toJSON() : null,
            phoneNumber: this._phoneNumber,
            sapVendorNumber: this._sapVendorNumber
        }
    }
};