export default class AddPartnerRepReq {

    _firstName:string;

    _lastName:string;

    _emailAddress:string;

    _accountId:string;

    constructor(firstName:string,
                lastName:string,
                emailAddress:string,
                accountId:string) {

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!emailAddress) {
            throw new TypeError('emailAddress required');
        }
        this._emailAddress = emailAddress;

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

    }

    get firstName():string {
        return this._firstName;
    }

    get lastName():string {
        return this._lastName;
    }

    get emailAddress():string {
        return this._emailAddress;
    }

    get accountId():string {
        return this._accountId;
    }

    toJSON() {
        return {
            firstName: this._firstName,
            lastName: this._lastName,
            emailAddress: this._emailAddress,
            accountId: this._accountId
        };
    }
}