import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import W9InfoView from './w9InfoView';
import W9InfoViewFactory from './w9InfoViewFactory';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class IsW9InfoExistsForPartnerRepsFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * gets the w9 info with the provided ids.
     * @param {string[]} partnerRepIds
     * @param {string} accessToken
     * @returns {Promise.<W9InfoView[]>}
     */
    execute(partnerRepIds:string[],
            accessToken:string):Promise<W9InfoView[]> {

        return this._httpClient
            .createRequest(`/partner-reps/w9info`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withHeader('Content-Type', `application/json`)
            .withContent(JSON.stringify(partnerRepIds))
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        new W9InfoView(
                            contentItem.id,
                            contentItem.isExists
                        )
                )
            );
    }
}

export default IsW9InfoExistsForPartnerRepsFeature;