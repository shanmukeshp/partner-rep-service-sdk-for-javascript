import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import BankInfoView from './bankInfoView';
import BankInfoViewFactory from './bankInfoViewFactory';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class IsBankInfoExistsForPartnerRepsFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * gets the bank info with the provided ids.
     * @param {string[]} partnerRepIds
     * @param {string} accessToken
     * @returns {Promise.<BankInfoView[]>}
     */
    execute(partnerRepIds:string[],
            accessToken:string):Promise<BankInfoView[]> {

        return this._httpClient
            .createRequest(`/partner-reps/bankinfo`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withHeader('Content-Type', `application/json`)
            .withContent(JSON.stringify(partnerRepIds))
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        new BankInfoView(
                            contentItem.id,
                            contentItem.isExists
                        )
                )
            );
    }
}

export default IsBankInfoExistsForPartnerRepsFeature;