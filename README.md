## Description
Precor Connect partner rep service SDK for javascript.

## Features

#####Add Partner Rep
* [documentation](src/root/api/features/AddPartnerRep.feature)

#####Get Partner Rep Bank Info Update URL
* [documentation](features/GetPartnerRepBankInfoUpdateUrl.feature)

#####Get Partner Rep W9 Update URL
* [documentation](features/GetPartnerRepW9UpdateUrl.feature)

#####Get Partner Rep With Id
* [documentation](features/GetPartnerRepWithId.feature)

#####Get Partner Reps With Ids
* [documentation](features/GetPartnerRepsWithIds.feature)
 
#####Search For Partner Rep With Email Address
* [documentation](features/SearchForPartnerRepWithEmailAddress.feature)

#####Update Partner Rep First Name
* [documentation](features/UpdatePartnerRepFirstName.feature)

#####Update Partner Rep Last Name
* [documentation](features/UpdatePartnerRepLastName.feature)

#####Update Partner Rep Phone Number
* [documentation](features/UpdatePartnerRepPhoneNumber.feature)

#####Update Partner Rep Postal Address
* [documentation](features/UpdatePartnerRepPostalAddress.feature)

## Setup

**install via jspm**  
```shell
jspm install partner-rep-service-sdk=bitbucket:precorconnect/partner-rep-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import PartnerRepServiceSdk,{PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk'

const partnerRepServiceSdkConfig = 
    new PartnerRepServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const partnerRepServiceSdk = 
    new PartnerRepServiceSdk(
        partnerRepServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```